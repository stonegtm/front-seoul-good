import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _0886f736 = () => interopDefault(import('../pages/best_seller.vue' /* webpackChunkName: "pages/best_seller" */))
const _416506fd = () => interopDefault(import('../pages/checkout/index.vue' /* webpackChunkName: "pages/checkout/index" */))
const _fd72f836 = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _2fd54661 = () => interopDefault(import('../pages/newarrival-all.vue' /* webpackChunkName: "pages/newarrival-all" */))
const _d832f0f8 = () => interopDefault(import('../pages/privacy.vue' /* webpackChunkName: "pages/privacy" */))
const _1d2ce44b = () => interopDefault(import('../pages/privacy-policy.vue' /* webpackChunkName: "pages/privacy-policy" */))
const _be6a753c = () => interopDefault(import('../pages/promotion/index.vue' /* webpackChunkName: "pages/promotion/index" */))
const _a9806252 = () => interopDefault(import('../pages/register.vue' /* webpackChunkName: "pages/register" */))
const _4d7e998f = () => interopDefault(import('../pages/review/index.vue' /* webpackChunkName: "pages/review/index" */))
const _42cb4a77 = () => interopDefault(import('../pages/seoulgoodspick.vue' /* webpackChunkName: "pages/seoulgoodspick" */))
const _7ff16a9f = () => interopDefault(import('../pages/auth/login_facebook.vue' /* webpackChunkName: "pages/auth/login_facebook" */))
const _074f0f8b = () => interopDefault(import('../pages/review/write_review.vue' /* webpackChunkName: "pages/review/write_review" */))
const _6ddb2d64 = () => interopDefault(import('../pages/user/address.vue' /* webpackChunkName: "pages/user/address" */))
const _6aeb8ece = () => interopDefault(import('../pages/user/profile.vue' /* webpackChunkName: "pages/user/profile" */))
const _5b031d7e = () => interopDefault(import('../pages/user/purchase.vue' /* webpackChunkName: "pages/user/purchase" */))
const _9b01dfb2 = () => interopDefault(import('../pages/review/main/_name.vue' /* webpackChunkName: "pages/review/main/_name" */))
const _a0dbd044 = () => interopDefault(import('../pages/brand/_name.vue' /* webpackChunkName: "pages/brand/_name" */))
const _35aba166 = () => interopDefault(import('../pages/category/_name.vue' /* webpackChunkName: "pages/category/_name" */))
const _c35cc3f2 = () => interopDefault(import('../pages/product/_product/index.vue' /* webpackChunkName: "pages/product/_product/index" */))
const _4406c3fa = () => interopDefault(import('../pages/promotion/_name.vue' /* webpackChunkName: "pages/promotion/_name" */))
const _1e8acfb2 = () => interopDefault(import('../pages/review/_name.vue' /* webpackChunkName: "pages/review/_name" */))
const _531f2e24 = () => interopDefault(import('../pages/searching/_search.vue' /* webpackChunkName: "pages/searching/_search" */))
const _7bb3a208 = () => interopDefault(import('../pages/successfuly/_ordernumber.vue' /* webpackChunkName: "pages/successfuly/_ordernumber" */))
const _90c23e64 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/best_seller",
    component: _0886f736,
    name: "best_seller"
  }, {
    path: "/checkout",
    component: _416506fd,
    name: "checkout"
  }, {
    path: "/login",
    component: _fd72f836,
    name: "login"
  }, {
    path: "/newarrival-all",
    component: _2fd54661,
    name: "newarrival-all"
  }, {
    path: "/privacy",
    component: _d832f0f8,
    name: "privacy"
  }, {
    path: "/privacy-policy",
    component: _1d2ce44b,
    name: "privacy-policy"
  }, {
    path: "/promotion",
    component: _be6a753c,
    name: "promotion"
  }, {
    path: "/register",
    component: _a9806252,
    name: "register"
  }, {
    path: "/review",
    component: _4d7e998f,
    name: "review"
  }, {
    path: "/seoulgoodspick",
    component: _42cb4a77,
    name: "seoulgoodspick"
  }, {
    path: "/auth/login_facebook",
    component: _7ff16a9f,
    name: "auth-login_facebook"
  }, {
    path: "/review/write_review",
    component: _074f0f8b,
    name: "review-write_review"
  }, {
    path: "/user/address",
    component: _6ddb2d64,
    name: "user-address"
  }, {
    path: "/user/profile",
    component: _6aeb8ece,
    name: "user-profile"
  }, {
    path: "/user/purchase",
    component: _5b031d7e,
    name: "user-purchase"
  }, {
    path: "/review/main/:name?",
    component: _9b01dfb2,
    name: "review-main-name"
  }, {
    path: "/brand/:name?",
    component: _a0dbd044,
    name: "brand-name"
  }, {
    path: "/category/:name?",
    component: _35aba166,
    name: "category-name"
  }, {
    path: "/product/:product",
    component: _c35cc3f2,
    name: "product-product"
  }, {
    path: "/promotion/:name",
    component: _4406c3fa,
    name: "promotion-name"
  }, {
    path: "/review/:name",
    component: _1e8acfb2,
    name: "review-name"
  }, {
    path: "/searching/:search?",
    component: _531f2e24,
    name: "searching-search"
  }, {
    path: "/successfuly/:ordernumber?",
    component: _7bb3a208,
    name: "successfuly-ordernumber"
  }, {
    path: "/",
    component: _90c23e64,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
