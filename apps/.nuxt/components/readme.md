# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<DialogRegister>` | `<dialog-register>` (components/dialogRegister.vue)
- `<Footer>` | `<footer>` (components/footer.vue)
- `<Login>` | `<login>` (components/login.vue)
- `<CheckoutAddress>` | `<checkout-address>` (components/checkout/address.vue)
- `<HomepageBestSellers>` | `<homepage-best-sellers>` (components/homepage/best-sellers.vue)
- `<HomepageCarousel>` | `<homepage-carousel>` (components/homepage/carousel.vue)
- `<HomepageFeaturedBrand>` | `<homepage-featured-brand>` (components/homepage/featured-brand.vue)
- `<HomepageNewArrivalOver>` | `<homepage-new-arrival-over>` (components/homepage/new-arrival-over.vue)
- `<HomepageNewArrival>` | `<homepage-new-arrival>` (components/homepage/new-arrival.vue)
- `<HomepageReview>` | `<homepage-review>` (components/homepage/review.vue)
- `<HomepageViewshowroom>` | `<homepage-viewshowroom>` (components/homepage/viewshowroom.vue)
- `<HeaderModuleAccount>` | `<header-module-account>` (components/header-module/account.vue)
- `<HeaderModuleCart>` | `<header-module-cart>` (components/header-module/cart.vue)
- `<HeaderModuleMainHeader>` | `<header-module-main-header>` (components/header-module/main-header.vue)
- `<HeaderModuleMenuSlide>` | `<header-module-menu-slide>` (components/header-module/menu_slide.vue)
- `<HeaderModuleSearch>` | `<header-module-search>` (components/header-module/search.vue)
- `<HeaderModuleToggle>` | `<header-module-toggle>` (components/header-module/toggle.vue)
- `<ProductMayAlsoLike>` | `<product-may-also-like>` (components/product/mayAlsoLike.vue)
- `<UserNavigation>` | `<user-navigation>` (components/user/navigation.vue)
