export { default as DialogRegister } from '../../components/dialogRegister.vue'
export { default as Footer } from '../../components/footer.vue'
export { default as Login } from '../../components/login.vue'
export { default as CheckoutAddress } from '../../components/checkout/address.vue'
export { default as HomepageBestSellers } from '../../components/homepage/best-sellers.vue'
export { default as HomepageCarousel } from '../../components/homepage/carousel.vue'
export { default as HomepageFeaturedBrand } from '../../components/homepage/featured-brand.vue'
export { default as HomepageNewArrivalOver } from '../../components/homepage/new-arrival-over.vue'
export { default as HomepageNewArrival } from '../../components/homepage/new-arrival.vue'
export { default as HomepageReview } from '../../components/homepage/review.vue'
export { default as HomepageViewshowroom } from '../../components/homepage/viewshowroom.vue'
export { default as HeaderModuleAccount } from '../../components/header-module/account.vue'
export { default as HeaderModuleCart } from '../../components/header-module/cart.vue'
export { default as HeaderModuleMainHeader } from '../../components/header-module/main-header.vue'
export { default as HeaderModuleMenuSlide } from '../../components/header-module/menu_slide.vue'
export { default as HeaderModuleSearch } from '../../components/header-module/search.vue'
export { default as HeaderModuleToggle } from '../../components/header-module/toggle.vue'
export { default as ProductMayAlsoLike } from '../../components/product/mayAlsoLike.vue'
export { default as UserNavigation } from '../../components/user/navigation.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
