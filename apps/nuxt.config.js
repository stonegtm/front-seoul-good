import colors from "vuetify/es5/util/colors";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s สวยง่าย จ่ายสะดวก สไตล์เกาหลี",
    title: "SEOULGOOD |",
    htmlAttrs: {
      lang: "th"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "description", content: "แหล่งรวมผลิตภัณฑ์เพื่อความงามแบบครบวงจรจากแบรนด์นำเข้าจากเกาหลี needly, seoulgood,skin veil, sulwhasoo" },
      { name: "keywords", content: "needly, seoulgood,skin veil, sulwhasoo" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/logo-seoulgood.ico" }],
  },
  server: {
    port: process.env.PORT || 3001 // default: 3000
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/css/main.css", "@/assets/css/checkout.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // '@/plugins/gtag'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/google-analytics',
    "@nuxtjs/vuetify"
  ],
  googleAnalytics: {
    id: 'G-7851RFYF1S'
  },
  publicRuntimeConfig: {
    googleAnalytics: {
      id: process.env.GOOGLE_ANALYTICS_ID || "G-7851RFYF1S"
    }
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "@nuxtjs/auth-next", '@nuxtjs/recaptcha'],
  recaptcha: {
    hideBadge: true, // Hide badge element (v3)
    siteKey: "6LdUEh4gAAAAADNXa5g5E-HcjCH4_9upaqy7q-fZ",    // Site key for requests
    version: 2,
  },
  publicRuntimeConfig: {
    recaptcha: {
      siteKey: "6LdUEh4gAAAAADNXa5g5E-HcjCH4_9upaqy7q-fZ" // for example
    }
  },

  auth: {
    plugins: [
      { src: "~/plugins/provider", ssr: false }
    ],
    strategies: {
      local: {
        token: {
          property: 'user.token',
          // global: true,
          // required: true,
          // type: 'Bearer'
        },
        user: {
          property: 'data',
          autoFetch: true
        },
        endpoints: {
          login: {
            url: "api/v1/login-customer",
            method: "post",
            // propertyName: "data.token"
          },
          user: {
            url: "api/v1/customer-data",
            method: "get",
            // propertyName: "data"
          },
          logout: false
        }
      },
      facebook: {
        endpoints: {
          userInfo:
            'https://graph.facebook.com/v2.12/me?fields=about,name,picture{url},email',
        },
        clientId: "488826255778182",
        scope: ['public_profile', 'email']
      }
      // google: {
      //   client_id:
      //     '583236818971-80qpvrvo6i27aljifh4okh9lf7dqlqjo.apps.googleusercontent.com'
      // }
    },
    redirect: {
      login: "/",
      callback: "/auth/login_facebook"
    }
  },
  axios: {
    proxy: true
  },
  proxy: {
    "/api": process.env.API_URL,
    "/brands/v1": process.env.API_URL,
    "/homepage/v1": process.env.API_URL,

  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    theme: {
      light: true,
      themes: {
        light: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
};
