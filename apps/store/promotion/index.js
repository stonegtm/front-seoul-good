export const state = () => ({});
export const mutations = {};

export const actions = {

    async getPromotion({ commit }) {
        const res = await this.$axios.$get("api/v1/front-show-data-promotion");
        return res;
    },
    async getPromotionSingle({ commit }, data) {
        const res = await this.$axios.$get(`api/v1/front-show-data-promotion-single/${data}`);
        return res;
    },
    async getProductPromotion({ commit }, data) {
        const res = await this.$axios.$post("api/v1/front-show-product-promotion", data);
        return res;
    },


};
