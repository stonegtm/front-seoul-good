export const state = () => ({});
export const mutations = {};
export const actions = {
  async profileUpdate({ commit }, data) {
    const res = await this.$axios.$post("api/v1/update-profile", data);
    return res;
  },
  async getAdress({ commit }) {
    const res = await this.$axios.$get("api/v1/get-address");
    return res;
  },
  async getAdressById({ commit }, data) {
    const res = await this.$axios.$post("api/v1/get-by-address", data);
    return res;
  },
  async newAdress({ commit }, data) {
    const res = await this.$axios.$post("api/v1/add-new-address", data);
    return res;
  },
  async updateAdress({ commit }, data) {
    const res = await this.$axios.$post("api/v1/update-address", data);
    return res;
  },
  async callProfile({ commit }) {
    const res = await this.$axios.$get("api/v1/get-profile");
    return res;
  }
};

export const getters = {};
