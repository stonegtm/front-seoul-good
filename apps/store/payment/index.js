export const state = () => ({});
export const mutations = {};
export const actions = {
  async getPayment({ commit }) {
    const res = await this.$axios.$get("api/v1/get-payment");
    return res;
  },
  async getSlipPic({ commit }, data) {
    const res = await this.$axios.$post("api/v1/get-slip-pic", data);
    return res;
  },
  async changeSlipPic({ commit }, data) {
    const formData = new FormData();
    formData.append("order_number", data.order_number);
    formData.append("image", data.image);
    formData.append("image_name", data.image_name);
    const res = await this.$axios.$post("api/v1/change-slip-pic", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    // const res = await this.$axios.$post("api/v1/change-slip-pic",data);
    return res;
  },


};

export const getters = {};
