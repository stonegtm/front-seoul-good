export const state = () => ({});
export const mutations = {};
export const actions = {
  async registerCustomers({ commit, state }, data) {
    const res = await this.$axios.$post("api/v1/register",data);
    return res;
  },
};

export const getters = {};
