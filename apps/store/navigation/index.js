export const state = () => ({
});
export const mutations = {

};

export const actions = {
    async searchProduct({ commit }) {
        const res = await this.$axios.$post("api/v1/search-product");
        return res;
    }
    , async searchProductPage({ commit }) {
        const res = await this.$axios.$post("api/v1/search-product-page");
        return res;
    }

};
