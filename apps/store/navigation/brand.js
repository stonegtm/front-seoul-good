export const state = () => ({
  categories: null
});
export const mutations = {

};

export const actions = {
  async selectBrand({ commit }) {
    const res = await this.$axios.$get("brands/v1/brands");
    // console.log(res);
    return res;
  }
};
