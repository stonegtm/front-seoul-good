export const state = () => ({
  categories: null
});
export const mutations = {
  CATEGORIES(state, payload) {
    state.categories = payload;
  }
};

export const actions = {
  async getCategory({ commit }) {
    const res = await this.$axios.$get("api/categories");
    return res;
  }
};
