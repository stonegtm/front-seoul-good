export const state = () => ({
  categories: null
});
export const mutations = {};

export const actions = {
  async selectProductNewarrival({ commit }) {
    const res = await this.$axios.$get("api/v1/product/new-arrival");
    // console.log(res);
    return res;
  },

  async productOtherSingle({ commit }, data) {
    const res = await this.$axios.$post("api/v1/product-other-sigle", data);
    return res
  }

};
