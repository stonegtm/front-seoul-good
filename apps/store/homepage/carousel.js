export const state = () => ({});
export const mutations = {};
export const actions = {
  async selectImage({ commit, state }, data) {
    const res = await this.$axios.$get("homepage/v1/carousel-image");
    return res;
  },
};

export const getters = {};
