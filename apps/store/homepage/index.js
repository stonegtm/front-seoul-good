export const state = () => ({});
export const mutations = {};

export const actions = {
  async getFeatureBrand({ commit }) {
    const res = await this.$axios.$get("api/v1/feature-front-brand7789c5");
    return res;
  },
  async getBestSellers({ commit }) {
    const res = await this.$axios.$get("api/v1/backend-picked-best-sellers");
    return res;
  },
  async getBestSellersAll({ commit }) {
    const res = await this.$axios.$get("api/v1/backend-picked-best-sellers-all");
    return res;
  }
};
