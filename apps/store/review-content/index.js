export const state = () => ({});
export const mutations = {};
export const actions = {
    async getReview({ commit }) {
        const res = await this.$axios.$get("api/v1/get-review");
        return res;
    }, async getReviewCustomer({ commit }) {
        const res = await this.$axios.$get("api/v1/show-review-customer");
        return res;
    },
    async getReviewSingle({ commit }, data) {
        const res = await this.$axios.$post("api/v1/get-review-single", data);
        return res;
    },
    async getReviewCustommerSingle({ commit }, data) {
        const res = await this.$axios.$post("api/v1/get-review-customer-single", data);
        return res;
    },
    async getProductReview({ commit }, data) {
        const res = await this.$axios.$post("api/v1/pick-product-review", data);
        return res;
    },
    async getFullName({ commit }, data) {
        const res = await this.$axios.$post("api/v1/get-name-user", data);
        return res;
    },
    async createReview({ commit }, data) {
        // const res = await this.$axios.$post("api/v1/create-review-customer", data);
        const formData = new FormData();
        formData.append("title", data.title);
        formData.append("title_sub", data.title_sub);
        formData.append("description", data.description);
        formData.append("product_id", data.product_id);
        formData.append("customer_id", data.customer_id);
        formData.append("image", data.image);
        formData.append("rating", data.rating);
        const res = await this.$axios.$post("api/v1/create-review-customer", formData, {
            headers: {
                "Content-Type": "multipart/form-data"
            }
        });
        return res
    },
};

export const getters = {};
