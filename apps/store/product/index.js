export const state = () => ({
});
export const mutations = {

};
export const actions = {
  async showProduct({ commit }, data) {
    const res = await this.$axios.$post("api/v1/product/product-single", data);
    // console.log(res);
    return res;
  },
  async dataProductOther({ commit }, data) {
    const res = await this.$axios.$post("api/v1/product/get-product-other", data);
    return res;
  },
};

export const getters = {

};
