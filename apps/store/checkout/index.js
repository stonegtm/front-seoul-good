export const state = () => ({});
export const mutations = {};
export const actions = {
  async selectProvince({ commit }) {
    const res = await this.$axios.$get("api/v1/province-for-address");
    return res;
  },
  async callAddress({ commit }) {
    const res = await this.$axios.$get("api/v1/address-for-press");
    return res;
  },
};

export const getters = {};
