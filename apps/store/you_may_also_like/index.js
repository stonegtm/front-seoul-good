export const state = () => ({});
export const mutations = {};

export const actions = {

  async getYouMayAlsoLike({ commit }) {
    const res = await this.$axios.$get("api/v1/front-you-may-also-like");
    return res;
  }
};
