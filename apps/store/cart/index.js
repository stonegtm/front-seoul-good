export const state = () => ({
  cart: [],
  favoriteProduct: []
});
export const mutations = {
  SET_CART(state, payload) {
    state.cart = payload;
  },
  SET_CART_LOGOUT(state) {
    state.cart = [];
    state.favoriteProduct = []
  },
  SET_FAVORITE_PRODUCT(state, payload) {
    state.favoriteProduct = payload;
  },
};
export const actions = {
  async checkout({ commit }, data) {
    if (data.image) {
      const formData = new FormData();
      formData.append("customer_id", data.customer_id);
      formData.append("status", data.status);
      formData.append("product_order", data.product_order);
      formData.append("address", data.address);
      formData.append("image", data.image);
      formData.append("sumPrice", data.sumPrice);
      const res = await this.$axios.$post("api/v1/checkout", formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      });
      if (res.success) {
        commit("SET_CART", res.data);
      }
      return res;
    }
  },
  async logout({ commit }) {
    const res = await this.$axios.$post("api/v1/logout-customer");
    commit("SET_CART_LOGOUT");
    return res;
  },
  async getCart({ commit }) {
    const res = await this.$axios.$get("api/v1/get-cart");
    commit("SET_CART", res.data);
    // console.log(res);
    commit("SET_FAVORITE_PRODUCT", res.favorite);
    return res;
  },
  async addItemNewarrivalToCart({ commit }, data) {
    const res = await this.$axios.$post("api/cart/v1/add-item-to-cart", data);
    if (res.success) {
      commit("SET_CART", res.data);
    }
    return res;
  },
  async updownQuantity({ commit }, data) {
    // console.log(data);
    const res = await this.$axios.$post("api/cart/v1/quantity-cart", data);
    // console.log(res, "api");
    commit("SET_CART", res.data);
    return res;
  },
  async deleteCart({ commit }, data) {
    const res = await this.$axios.$post("api/cart/v1/delete-order", data);
    commit("SET_CART", res.data);
    return res;
  }
  ,
  async favoriteProduct({ commit }, data) {
    const res = await this.$axios.$post("api/v1/favorite-product", data);
    commit("SET_FAVORITE_PRODUCT", res.data);
    return res;
  },
  async favoriteProductPicked({ commit }) {
    if (this.$auth.loggedIn) {
      const data = { customer_id: this.$auth.user.data.id }
      const res = await this.$axios.$post("api/v1/favorite-product-picked", data);
      commit("SET_FAVORITE_PRODUCT", res);
      return res;
    }
  },
};

export const getters = {
  cartdata(state) {
    return state.cart;
  },
  favoriteData(state) {
    return state.favoriteProduct;
  }
};
