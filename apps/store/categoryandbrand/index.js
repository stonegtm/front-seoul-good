export const state = () => ({});
export const mutations = {};
export const actions = {
    async getProductSingle({ commit }, data) {
        const res = await this.$axios.$get(`api/v1/category-by-codename/${data}`);
        return res;
    },
    async getProductSingleByBrand({ commit }, data) {
        const res = await this.$axios.$get(`api/v1/brand-by-codename/${data}`);
        return res;
    },
};

export const getters = {};
