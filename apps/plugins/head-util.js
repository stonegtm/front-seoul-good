export default (_, inject) => {
    inject('headUtil', data => ({
        htmlAttrs: {
            lang: data.lang
        },
        title: data.title,
        meta: [
            {
                hid: 'description',
                name: 'description',
                content: data.description || 'description'
            },
            {
                name: 'keywords',
                content: ['keyword01', 'keyword02', 'keyword03',]
            },
            {
                property: 'og:title',
                name: 'og:title',
                content: data.title || 'seould good สวยแน่ จ่ายง่าย สไตล์เกาหลี'
            },
            {
                property: 'og:description',
                name: 'og:description',
                content: data.description || 'seould good สวยแน่ จ่ายง่าย สไตล์เกาหลี'
            },
            {
                property: 'og:url', name: 'og:url', content: ''
            }
        ]
    })
    )
}