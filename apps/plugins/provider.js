//providers.js
export default async function ({ app }) {
    if (!app.$auth.loggedIn) {
        return
    } else {
        const auth = app.$auth;
        const authStrategy = auth.strategy.name;
        if (authStrategy === 'facebook') {
            console.log('facebook');
            let data2 = {
                fb_token: auth.user.id,
                first_name: auth.user.name,
                email: auth.user.email,
                name: auth.user.name,
                image: auth.user.picture
            }
            try {
                const response = await app.$axios.$post("api/v1/login-customer-facebook", data2);
                await auth.setStrategy('local');
                await auth.strategy.token.set("Bearer " + response.user.token);
                await auth.fetchUser();
                // console.log(response);
            } catch (e) {
                console.log(e);
            }
        } else if (authStrategy === 'google') {
            console.log('google');
            let dataGoogle = {
                google_token: auth.user.sub,
                first_name: auth.user.given_name,
                last_name: auth.user.family_name
            }
            try {
                const response = await app.$axios.$post("/api/oauth", dataGoogle);
                await auth.setStrategy('local');
                await auth.strategy.token.set("Bearer " + response.user.token);
                await auth.fetchUser();

            } catch (e) {
                console.log(e);
            }
        }

    }
}