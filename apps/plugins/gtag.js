import Vue from 'vue'
import VueGtag from 'vue-gtag'

Vue.use(VueGtag, {
  config: { id: 'G-7851RFYF1S' }
})

// import Vue from 'vue';
// import VueGtag from 'vue-gtag';

// export default ({ app }) => {
//   Vue.use(VueGtag, {
//     config: { id: 'G-7851RFYF1S' },
//     appName: 'app-name',
//   }, app.router);
// }